#Build instructions #

1. Execute the install_jars.sh script in the move2alf-war folder
2. Setup a local mysql with database, username and password to move2alf (or execute the setup.sql in move2alf-war/sql)
3. mvn clean install in the root of the project

#Installation instructions #
1. Get the move2alf.war in the downloads section, or build it yourself.
2. Follow the [installation instructions on the Xenit website](http://xenit.eu/move2alf/manual/Installation%20Instructions). 
